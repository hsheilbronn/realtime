---
marp: true
theme: default
paginate: true
---

# Elegoo Mapping Robot
## Realtime Systems WS2020

Carolin Vosseler

Ramu Rakesh

Martin Sigloch

<!-- footer: Realtime Systems | MSEM WS2020 -->

---

# Idea

The robot will drive around the room, find a wall, follow it and record its position.
At the end a map of the room can be optained via the serial connection.

![bg right:60% w:700](img/MapingRobot.png)

---

# Examples of the map

![h:500](img/template.svg)
![bg right h:500](img/template3d.svg)

---

# Sensors

- 2x ultrasonic distance sensor
  - used to find the wall
- 1x reed sensor + magnet on wheel to measure distance
- 1x compass sensor to detect direction

---

# Sensor tests

- Ultrasonic sensor is working
- Reed sensor is working
  (after finding a good place to put sensor and magnet)
- Compass sensor is *not yet* working
  first problem was pin conflicts with front ultrasonic sensor
  now, only returns constant values

---

# Project plan: Planing					

- 2020-10-30 find the idea
- 2020-11-03 defining the sensors for the idea
- 2020-11-18 create high level algorithm
- 2020-11-17 create milestoneplan
- 2020-12-31 create project documentation

---

# Project plan: Implementation (1/2)

2020-11-11 assign pins for all sensors
2020-11-03 assemble the Robot
2020-11-27 Run test programs for all sensors to check if we get plauible values
2020-11-30 Read sensor data
2020-11-26 create functions to drive
2020-11-27 rotate robot based on compass
2020-12-08 scan wall function (servo + front ultrasonic sensor)
2020-12-08 implement "find wall"
2020-12-03 implement "align with wall"
2020-12-15 implement "drive parallel to wall"

---

# Project plan: Implementation (2/2)

2020-12-15 position recording (reed sensor ticks + compass)
2020-12-08 implement check for "mapping complete?"
2020-11-08 create a map template
2020-12-13 print map to serial port
2020-12-13 testing and refining
2021-01-18 debugging
2021-01-25 prepare live presentation

---

# Project plan: Presentation

2020-11-17 create Intermediate presentation
2020-12-22 Document program parts with or after each implementation step
2021-01-24 Documentation

---

# Project plan: Milestones

2020-11-03 Milestone 1:  Finalize standard robot
2020-11-26 Milestone 2:  Invent project idea
2020-12-15 Milestone 3:  Implement idea
2021-01-19 Milestone 4:  Debugging & prepare live presentation
2021-01-26 Milestone 5: Documentation (upload to ilias)
2020-12-21 Milestone 4: Finalize programming

