---
marp: true
theme: default
paginate: true
---

# SPI Interface
## example AD-Converter MCP 3008 connected to ATMega128

Carolin Vosseler

Martin Sigloch

<!-- footer: Realtime Systems | MSEM WS2020 -->

---

# Agenda

- What is SPI

- What is the MCP 3008 AD-Converter

- How to use it with ATMega128 and Arduino

---

# What is SPI

SPI is a serial bus to connect microcontrollers and sensors.

* 4 wires *(more details later)*
    - **S**lave **S**elect (**SS**, CS, CE, ...)
    - **S**erial **Cl**oc**k** (**SCLK**, SCK, ...)
    - **M**aster **O**utput, **S**lave **I**nput (**MOSI**, SI, DOUT, DIN, ...)
    - **M**aster **I**nput, **S**lave **O**utput (**MISO**, SO, DOUT, DIN, ...)
* Master-slave architecture
    - Master (e.g. ATMega128) chooses which sensor to talk to
    - Master defines speed of communication


---

# SPI connection

- SPI connection to a single device
![](img/SPI_single_slave.svg)

* SPI with multiple sensors
![](img/SPI_three_slaves.svg)


<!-- footer: Image credit: en:User:Cburnett (https://commons.wikimedia.org/wiki/File:SPI_single_slave.svg), „SPI single slave“, (https://commons.wikimedia.org/wiki/File:SPI_three_slaves.svg), „SPI three slaves“, https://creativecommons.org/licenses/by-sa/3.0/legalcode-->

---

# SPI Modes

- 4 Modes
    - master and slave need compatible setting
    - **C**lock **POL**arity (0: idle low | 1: idle high)
    - **C**lock **PHA**se (0: read on leading edge | 1: on tailing edge)

![](img/SPI_timing.svg)


<!-- footer: Image credt: SPI_timing_diagram.svg: en:User:Cburnett derivative work: Jordsan (https://commons.wikimedia.org/wiki/File:SPI_timing_diagram2.svg), „SPI timing diagram2“, https://creativecommons.org/licenses/by-sa/3.0/legalcode -->


---

# SPI protocoll


![](img/spi.webp)

<!-- footer: https://alchitry.com/blogs/tutorials/serial-peripheral-interface-spi -->

---

# SPI on ATmega128 & Arduino

![width:500](img/pinout.spi.png)

![bg right width:700](img/arduino.spi.png)


<!-- footer: Realtime Systems | MSEM WS2020 -->

---

# SPI on ATMega128

Example in C from [datasheet](files/atmega128.datasheet.pdf) page 164:

```
void SPI_MasterInit(void)
{
    /* Set MOSI and SCK output, all others input */
    DDR_SPI = (1<<DD_MOSI)|(1<<DD_SCK);
    /* Enable SPI, Master, set clock rate fck/16 */
    SPCR = (1<<SPE)|(1<<MSTR)|(1<<SPR0);
}

void SPI_MasterTransmit(char cData)
{
    /* Start transmission */
    SPDR = cData;
    /* Wait for transmission complete */
    while(!(SPSR & (1<<SPIF)))
        ;
}
```

<!-- note: page 163: SPI interface has no automatic control of the SS line. -->
<!-- note: instead of blocking wait, can activate interrupt -->
<!-- footer: "" -->

---

# SPI on ATmega128

Example in Assembly from [datasheet](files/atmega128.datasheet.pdf) page 164:

```
SPI_MasterInit:
; Set MOSI and SCK output, all others input
ldi r17,(1<<DD_MOSI)|(1<<DD_SCK)
out DDR_SPI,r17
; Enable SPI, Master, set clock rate fck/16
ldi r17,(1<<SPE)|(1<<MSTR)|(1<<SPR0)
out SPCR,r17
ret

SPI_MasterTransmit:
; Start transmission of data (r16)
out SPDR,r16

Wait_Transmit:
; Wait for transmission complete
sbis SPSR,SPIF
rjmp Wait_Transmit
ret
```


---

# Analog to Digital Converter (ADC)

- Convert analog singnals to digital signals

# MCP 3008

![width:400 bg right:40%](img/mcp3008.png)

* V$_{DD}$: 2.7V - 5.5V
* Measurement: 0.25V - V$_{DD}$
* 8 channels
* 10 bit resolution
* 200k samples per second –> every 5 µs

<!-- footer: Realtime Systems | MSEM WS2020 -->

---

# Commands of the AD-Converter MCP 3008

Section 5 & 6 of [datasheet](files/mcp3008.datasheet.pdf) page 19+:

- Optional leading zeros
- 1 start bit
- ADC mode bit: single or differential
- 3 control bits to select input
- n bits response

![width:400 bg right](img/mcp3008.controlbits.png)

---

#  AD-Converter MCP 3008 connected to Arduino

![](img/mcp3008.wireing.png)

<!-- Wires from purple to green: SCLK, MISO, MOSI, SS -->
<!-- red and blue wires are VCC and GND -->
<!-- red wire on the right is analog input -->


<!-- 
Interrupt Vectors:
18 $0022 SPI, STC SPI Serial Transfer Complete

Interface -SPI:
atmega128.Datasheet.full.pdf page 162+

Example code: SPI master:
page 164
-->

<!-- footer : http://domoticx.com/ic-mcp3008-a-d-converter/ -->

---

# Libraries

MCP3008 Library for Arduino: [https://github.com/adafruit/Adafruit_MCP3008](https://github.com/adafruit/Adafruit_MCP3008/blob/master/examples/simpletest/simpletest.ino)


```
#include <Adafruit_MCP3008.h>

Adafruit_MCP3008 adc;

int count = 0;

void setup() {
  Serial.begin(9600); while (!Serial);

  adc.begin(); // <- Setup ADC with default pins (Hardware SPI) 
}

void loop() {
  for (int chan=0; chan<8; chan++) {
    Serial.print(
        adc.readADC(chan) // <- read adc value
    ); Serial.print("\t");
  }
  Serial.print("["); Serial.print(count); Serial.println("]");
  count++;
  delay(1000);
}
```

<!-- footer: "" -->

---

# Thank you for your attention!

# Questions?

<!-- footer: Realtime Systems | MSEM WS2020 -->