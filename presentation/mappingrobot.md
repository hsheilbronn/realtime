---
marp: true
theme: default
paginate: true
---

# Elegoo Mapping Robot
## Realtime Systems WS2020

Carolin Vosseler

Ramu Rakesh

Martin Sigloch

![bg](img/template3d-bg.svg)

<!-- footer: Realtime Systems | MSEM WS2020 -->

---

# Idea

The robot will drive around the room, find a wall, follow it and record its position.
At the end a map of the room can be optained via the serial connection.

![bg right:60% w:700](img/MapingRobot.png)

---

# Wiring

Without VCC and GND connections

![width:100% bg right:65%](img/wiring.png)

---

# Hardware

![width:500](img/attach_reedsensor.jpeg)

---

# Demo

TODO: embedded video

---

