#!/usr/bin/env bash

echo -n "0,0"
cat $1.log \
  | grep -e "^[$][-0-9.][0-9.]*,[-0-9.][0-9.]*$" \
  | sed 's/^[$]/ /' \
  | tr -d '\n'

echo