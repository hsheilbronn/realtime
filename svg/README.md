# Maps and logs

This directory contains map templates, logs and generated maps.

## First tests

![](2020-12-26.svg)

![](2020-12-27.01.svg)

## First plausible track

This test was only using the functions findWall and alignWall.
FollowWall was disabled.
The robot was actually driving towards the middle of the room at the end.

![](2020-12-27.02.svg)
