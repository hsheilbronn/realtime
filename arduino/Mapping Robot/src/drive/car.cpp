

#include "drive/car.h"
#include <timer.h>

#define MAX_DIR_DIFF 5
#define MIN_TIME_DIRECTION_REACHED_MS 200

float angle_distance(float a, float b) {
  float diff = a - b;
  while (diff < -180) {
    diff += 360;
  }
  while (diff > 180) {
    diff -= 360;
  }

  return diff;
}

Car::Car(Drive *drive, Compass *compass) {
  this->drive = drive;
  this->compass = compass;
  this->msTimer = Timer(MIN_TIME_DIRECTION_REACHED_MS);
  this->wasWrong = false;
}

bool Car::look(float direction, bool inPlace) {
  float diff = angle_distance(compass->getValue(), direction);
  if (abs(diff) > MAX_DIR_DIFF) {
    // linear decrese of speed, maximum if diff is over 90°
    float speed = diff / 90;
    if (inPlace) {
      drive->rotate(speed);
    } else {
      drive->curve(abs(speed), speed > 0 ? 1 : -1);
    }
    this->msTimer.reset();
    wasWrong = true;
  } else if (!wasWrong) {
    return true;
  } else {
    drive->stop();
    if (!inPlace || this->msTimer.run()) {
      wasWrong = false;
      return true;
    }
  }
  return false;
}

bool Car::driveTo(Position *target, Position *current) {
  float dx = target->x - current->x;
  float dy = target->y - current->y;
  float direction = atan2(dx, dy) / M_PI * 180.0 - 180;
  float distance = sqrt(dx * dx + dy * dy); // a² + b² = c²

  if (distance < 10) {
    drive->stop();
    return true;
  }
  if (look(direction)) {
    drive->drive(1.0);
  }
  return false;
}