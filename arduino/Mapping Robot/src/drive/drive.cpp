#include "drive/drive.h"
#include "config.h"
#include "leds.h"
#include "pinconfig.h"
#include <Arduino.h>
#include <PinChangeInterrupt.h>

static long tickCount = 0;
enum { TICK_OFF = 0, TICK_FORWARD = 1, TICK_BACKWARD = -1 } tickMode;

void reed_handler(void);

void Drive::setup(void) {
  tickMode = TICK_OFF;
  pinMode(PIN_MOTOR_LEFT, OUTPUT);
  pinMode(PIN_MOTOR_LEFT_FORWARD, OUTPUT);
  pinMode(PIN_MOTOR_LEFT_BACKWARD, OUTPUT);
  pinMode(PIN_MOTOR_RIGHT, OUTPUT);
  pinMode(PIN_MOTOR_RIGHT_FORWARD, OUTPUT);
  pinMode(PIN_MOTOR_RIGHT_BACKWARD, OUTPUT);

  pinMode(PIN_REED, INPUT);

  attachPCINT(digitalPinToPCINT(PIN_REED), reed_handler, CHANGE);
}

void reed_handler(void) {
  switch (tickMode) {
  case TICK_FORWARD:
    tickCount++;
    break;
  case TICK_BACKWARD:
    tickCount--;
    break;
  default:
    break;
  }
  led_set(LED_REED, 1 + tickCount);
}

/**
 * Set motor speed
 * left and right can be values between -0.1 (full speed backwards)
 *                             over 0.0 (stop) and 1.0 (full speed)
 */
void Drive::move(float left, float right) {
  // Limit and scale values
  // analogWrite taks values from 0 upto 255
  left = constrain(left, -1.0, 1.0);
  right = constrain(right, -1.0, 1.0);
  int pwmL = (int)(abs(left) * (PWM_MAX - PWM_MIN) + PWM_MIN);
  int pwmR = (int)(abs(right) * (PWM_MAX - PWM_MIN) + PWM_MIN);

  analogWrite(PIN_MOTOR_LEFT, pwmL);
  digitalWrite(PIN_MOTOR_LEFT_FORWARD, (left > 0.0 ? HIGH : LOW));
  digitalWrite(PIN_MOTOR_LEFT_BACKWARD, (left < 0.0 ? HIGH : LOW));

  analogWrite(PIN_MOTOR_RIGHT, pwmR);
  digitalWrite(PIN_MOTOR_RIGHT_BACKWARD, (right < 0.0 ? HIGH : LOW));
  digitalWrite(PIN_MOTOR_RIGHT_FORWARD, (right > 0.0 ? HIGH : LOW));

  if (left >= 0.0 && right >= 0.0) {
    // moving forward or standing
    tickMode = TICK_FORWARD;
  } else if (left <= 0.0 && right <= 0.0) {
    // moving backwards
    tickMode = TICK_BACKWARD;
  } else {
    // rotating in place
    tickMode = TICK_OFF;
  }
}

void Drive::stop() { Drive::move(0.0, 0.0); }

void Drive::drive(float speed) { Drive::move(speed, speed); }

void Drive::rotate(float speed) { Drive::move(speed, -speed); }

void Drive::curve(float speed, float curve) {
  float diff = constrain(abs(speed * curve), 0.0, speed);
  if (curve > 0) {
    Drive::move(speed, speed - diff);
  } else {
    Drive::move(speed - diff, speed);
  }
}

float Drive::distance(bool reset) {
  // Wheel diameter: 6.5cm
  // circumference: PI * 6.5cm = 20.4cm
  // ticks per revolution: 6
  // distance per tick: 20.4cm / 6 = 3.4cm
  return ((float)ticks(reset)) * 3.4;
}

long Drive::ticks(bool reset) {
  noInterrupts();
  long out = tickCount;
  if (reset) {
    tickCount = 0;
  }
  interrupts();
  return out;
}