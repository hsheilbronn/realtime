// include guard to prevent file to be included twice
#pragma once


/**
 * Functions to move the robot around
 */
class Drive {
public:
  void setup(void);

  // Move with specific speeds for left and right side
  void move(float left, float right);

  void stop(void);
  void drive(float speed = 1);
  void rotate(float speed);
  void curve(float speed, float curve);

  long ticks(bool reset = false);
  float distance(bool reset = false);
};