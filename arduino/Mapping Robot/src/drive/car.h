#pragma once

#include "compass/compass.h"
#include "drive/drive.h"
#include <map.h>
#include <timer.h>

#define MAX_DIR_DIFF 5
#define MIN_TIME_DIRECTION_REACHED_MS 200

class Car {
  Drive *drive;
  Compass *compass;
  Timer msTimer;
  bool wasWrong;

public:
  Car(Drive *drive, Compass *compass);

  /**
   * @param direction direction to turn the car towards in degree
   */
  bool look(float direction, bool inPlace = true);
  bool driveTo(Position *target, Position *current);
};