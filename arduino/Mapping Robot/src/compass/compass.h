#pragma once

#define DIR_NORTH 270

class Compass {
public:
  Compass(void) { resetCalibration(); }
  void setup(void);
  float getValue(void);
  unsigned long count(bool reset = false);
  unsigned long count2(bool reset = false);
  void resetCalibration(void) {
    xMin = 0;
    xMax = 0;
    yMin = 0;
    yMax = 0;
  }

private:
  unsigned long counter;
  unsigned long counter2;
  // callibration values
  int xMin;
  int xMax;
  int yMin;
  int yMax;
};