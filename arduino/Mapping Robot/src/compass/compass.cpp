
#include "compass/compass.h"
#include <Arduino.h>
#include <Wire.h>

#define COMPASS_ADDRESS 0x1E // address of HMC5883

void Compass::setup(void) {
  Wire.begin(); // as master
  Wire.beginTransmission(COMPASS_ADDRESS);
  Wire.write(0x02); // Set mode...
  Wire.write(0x00); // ... to continuous
  Wire.endTransmission();
}

float Compass::getValue(void) {
  static float cache = 0;
  static unsigned long nextFetch = 0;

  if (millis() >= nextFetch) {
    nextFetch = millis() + 60;

    int x, y, z;
    Wire.beginTransmission(COMPASS_ADDRESS);
    Wire.write(0x03); // Register 3, X MSB Register
    Wire.endTransmission();
    Wire.requestFrom(COMPASS_ADDRESS, 6);
    if (6 <= Wire.available()) {
      x = Wire.read() << 8; // X msb
      x |= Wire.read();     // X lsb
      z = Wire.read() << 8; // Z msb
      z |= Wire.read();     // Z lsb
      y = Wire.read() << 8; // Y msb
      y |= Wire.read();     // Y lsb
    }

    // update callibration values
    xMin = min(xMin, x);
    xMax = max(xMax, x);
    yMin = min(yMin, y);
    yMax = max(yMax, y);

    x -= ((xMax + xMin) / 2);
    y -= ((yMax + yMin) / 2);

    float w = atan2(-y, x) / M_PI * 180.0; // calculate angle in degree
    if (w != cache) {
      cache = w;
      this->counter++;
    } else {
      this->counter2++;
    }
  }
  return cache;
}

unsigned long Compass::count(bool reset) {
  unsigned long out = this->counter;
  if (reset) {
    this->counter = 0;
  }
  return out;
}

unsigned long Compass::count2(bool reset) {
  unsigned long out = this->counter2;
  if (reset) {
    this->counter2 = 0;
  }
  return out;
}