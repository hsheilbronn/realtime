#pragma once

#include <Arduino.h>

#define STATE_SIZE 20

int compare(const void *a, const void *b) {
  float fa = *(float *)a;
  float fb = *(float *)b;
  if (fa > fb)
    return 1;
  if (fa < fb)
    return -1;
  return 0;
}

class Filter {
private:
  float state[STATE_SIZE];
  unsigned int index;
  float sorted[STATE_SIZE];

public:
  Filter(float init = 0) {
    for (unsigned int i = 0; i < STATE_SIZE; i++) {
      this->state[i] = init;
      this->sorted[i] = init;
    }
    this->index = 0;
  }

  void add(float next) {
    this->state[this->index] = next;
    this->index = (this->index + 1) % STATE_SIZE;
  }

  float get(void) {
    memcpy(sorted, state, sizeof(state));
    qsort(sorted, STATE_SIZE, sizeof(sorted[0]), &compare);
    return sorted[STATE_SIZE / 2];
  }

  float median(float next) {
    this->add(next);
    return this->get();
  }
};
