#pragma once

class Side {
public:
  void setup(void);
  void trigger(void);
  float lastValue(void);
  float lastMinValue(void);
  unsigned long count(bool reset = false);
};