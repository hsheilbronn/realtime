/**
 *
 * Arduino UNO only supports PinInterrupts on two pins.
 *
 *
 * References:
 * - https://github.com/NicoHood/PinChangeInterrupt
 */

#include "front.h"
#include "pinconfig.h"
#include <Arduino.h>
#include <PinChangeInterrupt.h>
#include <Servo.h>

static Servo myservo;

static unsigned long front_trigger_time = 0;
static unsigned long front_echo_time = 0;
static unsigned long front_next_trigger = 0;
static float front_distance = 99.99;
static unsigned long front_count = 0;

#define POS_CENTER 85

void front_echo_handler(void);

void Front::setup(void) {
  // Setup the servo
  myservo.attach(PIN_FRONT_SERVO);

  // servo test
  for (int i = 0; i < 2; i++) {
    look(-1);
    delay(300);
    look(1);
    delay(300);
  }
  look(0); // point senor forward

  pinMode(PIN_FRONT_TRIGGER, OUTPUT);
  pinMode(PIN_FRONT_ECHO, INPUT);

  attachPCINT(digitalPinToPCINT(PIN_FRONT_ECHO), front_echo_handler, CHANGE);
}

/**
 * Trigger the ultrasonic sensor
 * This will block for ~15 microseconds
 */
void Front::trigger(void) {
  if (micros() < front_next_trigger) {
    return;
  }

  front_next_trigger = micros() + 100000;

  digitalWrite(PIN_FRONT_TRIGGER, LOW);
  delayMicroseconds(2);
  // Setting the triggerpin high for 10us to generate a wave
  digitalWrite(PIN_FRONT_TRIGGER, HIGH);
  delayMicroseconds(10);
  digitalWrite(PIN_FRONT_TRIGGER, LOW);
  front_trigger_time = 0;
}

void front_echo_handler(void) {
  if (digitalRead(PIN_FRONT_ECHO)) {
    front_trigger_time = micros();
    front_echo_time = 0;
  } else if (front_trigger_time == 0) {
    // shuldn't happen, ignore
  } else {
    front_echo_time = micros() - front_trigger_time;
    front_trigger_time = 0;
    front_next_trigger = micros() + 400;

    float current = (float)(front_echo_time * 0.01715);
    front_distance = current;//(front_distance * 0.6) + (current * 0.4);
    front_count++;
  }
}

float Front::lastValue(void) { return front_distance; }

unsigned long Front::count(bool reset) {
  noInterrupts();
  unsigned long out = front_count;
  if (reset) {
    front_count = 0;
  }
  interrupts();
  return out;
}

/**
 * Look in a direction between -1 (left) 0 (forward) and 1 (right)
 * without parameter this will oscillate between -1 and 1
 */
void Front::look(float direction) {
  if (direction == FRONT_LOOK_LEFT_RIGHT) {
    direction = sin(millis() * 0.02);
  }
  int dir = constrain(direction, -1, 1) * FRONT_ANGLE;
  myservo.write(POS_CENTER + dir);
}
