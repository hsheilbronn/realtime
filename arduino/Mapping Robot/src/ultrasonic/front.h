#pragma once

#define FRONT_ANGLE 45
#define FRONT_LOOK_LEFT_RIGHT 12345 // oscillate between left and right

class Front {
public:
  void setup(void);
  void trigger(void);
  float lastValue(void);
  unsigned long count(bool reset = false);
  void look(float direction = FRONT_LOOK_LEFT_RIGHT);
};