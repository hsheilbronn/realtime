/**
 * Ultrasonic sensor on side of the robot
 *
 * References:
 * - https://github.com/NicoHood/PinChangeInterrupt
 */

#include "side.h"
#include "filter.h"
#include "pinconfig.h"
#include <Arduino.h>
#include <PinChangeInterrupt.h>
#include <stdlib.h>
#include <timer.h>

static unsigned long side_trigger_time = 0;
static unsigned long side_echo_time = 0;
static unsigned long side_next_trigger = 0;
static float side_distance = 999.99;
static float side_min = 999.99;
static float side_min_out = 999.99;
static unsigned long side_count = 0;
static Timer minTimer = Timer(150);

// static Filter filter = Filter();

void side_echo_handler(void);

void Side::setup(void) {
  pinMode(PIN_SIDE_TRIGGER, OUTPUT);
  pinMode(PIN_SIDE_ECHO, INPUT);

  attachPCINT(digitalPinToPCINT(PIN_SIDE_ECHO), side_echo_handler, CHANGE);
}

/**
 * Trigger the ultrasonic sensor
 * This will block for ~15 microseconds
 */
void Side::trigger(void) {
  if (micros() < side_next_trigger) {
    return;
  }

  side_next_trigger = micros() + 100000;

  digitalWrite(PIN_SIDE_TRIGGER, LOW);
  delayMicroseconds(2);
  // Setting the triggerpin high for 10us to generate a wave
  digitalWrite(PIN_SIDE_TRIGGER, HIGH);
  delayMicroseconds(10);
  digitalWrite(PIN_SIDE_TRIGGER, LOW);
  side_trigger_time = 0;
}

void side_echo_handler(void) {
  if (digitalRead(PIN_SIDE_ECHO)) {
    side_trigger_time = micros();
    side_echo_time = 0;
  } else if (side_trigger_time == 0) {
    // shuldn't happen, ignore
  } else {
    side_echo_time = micros() - side_trigger_time;
    side_trigger_time = 0;
    side_next_trigger = micros() + 400;
    float current = (float)(side_echo_time * 0.01715);
    side_distance = (side_distance * 0.98) + (current * 0.02);
    if (minTimer.run()) {
      side_min_out = side_min;
      side_min = 99.99;
    } else {
      side_min = min(side_min, side_distance);
    }
    side_count++;
  }
}

float Side::lastValue(void) { return side_distance; }
float Side::lastMinValue(void) { return min(side_min_out, side_min); }

unsigned long Side::count(bool reset) {
  noInterrupts();
  unsigned long out = side_count;
  if (reset) {
    side_count = 0;
  }
  interrupts();
  return out;
}