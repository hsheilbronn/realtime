#include <Arduino.h>

#include "alignwall.h"
#include "compass/compass.h"
#include "drive/car.h"
#include "drive/drive.h"
#include "followwall.h"
#include "leds.h"
#include "logger.h"
#include "map.h"
#include "ultrasonic/front.h"
#include "ultrasonic/side.h"
#include "watchdog.h"
#include <Wire.h>
#include <timer.h>

static Front front = Front();
static Side side = Side();
static Compass compass = Compass();
static Map myMap = Map();
static Drive drive = Drive();
static Car car = Car(&drive, &compass);
static Log logger = Log();

void setup() {
  // setup code, to run once:

  Serial.begin(9600); // use 9600 for bluetooth communication
  // Serial.begin(115200); // faster -> spend less time on Serial.print
  Serial.println(F("setup"));
  delay(500);

  front.setup();
  side.setup();

  if (!logger.init()) {
    front.look(1);
    while (true) {
      // don't start if SD card enabled is unavailable
      // (see ENABLE_SD in logger.h to enable/disable SD card)
    }
  }

  compass.setup();

  drive.setup();

  led_setup();

  Serial.println(F("WAIT_START)"));
  delay(300);

  setupWatchdog();
}

typedef enum {
  WAIT_START = 0,
  COMPASS_CALIBRATE,
  FIND_WALL,
  ALIGN_WALL,
  FOLLOW_WALL,
  MAPPING_DONE,
  WAYPOINTS,
} mainStep_t;

void loop() {
  // main code, to run repeatedly:
  static mainStep_t state = WAIT_START;
  static Timer msTimer = Timer();
  static Timer ledTimer = Timer(150);

  // Debug output
  static unsigned long cycle = 0;
  static unsigned long cycleStart = micros();
  static unsigned long cycleTotal = 0;
  static Timer statsTimer = Timer(1500);
  static unsigned long cycles = 0;
  static unsigned long cycleMax = 0;
  static unsigned long cycleMin = 9999;
  static unsigned long cycleMaxUnderMs = 0;
  static unsigned long cycleOverMs = 0;
  static bool showStats = false;
  unsigned long cycleTime = micros() - cycleStart;
  if (cycleTime > 1000) {
    cycleOverMs++;
  } else {
    cycleMaxUnderMs = max(cycleMaxUnderMs, cycleTime);
  }
  cycles++;
  cycleStart = micros();
  cycleTotal += cycleTime;
  cycleMax = max(cycleMax, cycleTime);
  cycleMin = min(cycleMin, cycleTime);
  const Position *pos = myMap.move(compass.getValue(), drive.distance(true));
  if (pos != NULL) {
    logger.logPos(pos);
  }
  if (showStats && statsTimer.run()) {
    Serial.println(("Stats:"));
    logger.p(F("cycle"), cycle, unit_count);
    logger.p(F("cycles"), cycles, unit_count);
    logger.p(F("c ø"), cycleTotal / cycles, unit_micros);
    logger.p(F("c min"), cycleMin, unit_micros);
    logger.p(F("c max"), cycleMax, unit_micros);
    logger.p(F("c >1k"), cycleOverMs, unit_count);
    logger.p(F("c <1k"), cycleMaxUnderMs, unit_micros);
    logger.p(F("front"), front.lastValue(), "cm");
    logger.p(F("front"), front.count(true), unit_count);
    resetWatchDogTimer();
    logger.p(F("side"), side.lastValue(), "cm");
    logger.p(F("side min"), side.lastMinValue(), "cm");
    logger.p(("side"), side.count(true), unit_count);
    logger.p(F("ticks"), drive.ticks(), unit_count);
    logger.p(F("distance"), drive.distance(), unit_count);
    logger.p(F("reed"), digitalRead(PIN_REED) ? 1 : 0, "");
    logger.p(F("compass"), compass.getValue(), "°");
    logger.p(("compass"), compass.count(true), unit_count);
    logger.p(("compass err"), compass.count2(true), unit_count);
    resetWatchDogTimer();
    myMap.print();
    logger.p(("serial print"), micros() - cycleStart, unit_micros);

    Serial.println();

    cycles = 0;
    cycleOverMs = 0;
    cycleMin = 9999;
    cycleMax = 0;
    cycleTotal = 0;
    resetWatchDogTimer();
  }
  cycleStart = micros();
  cycle++;

  if (ledTimer.run()) {
    led_apply();
  }

  front.trigger();
  side.trigger();
  compass.getValue(); // TODO: reading compass values takes more than 1ms

  led_front(front.lastValue());
  led_side(side.lastValue());

  // Record robot path and wall trace

  switch (state) {

  case WAIT_START: {
    drive.rotate(1);
    if (msTimer.run(3000)) {
      drive.stop();
      state = COMPASS_CALIBRATE;
      Serial.println(F("COMPASS_CALIBRATE)"));
      // state = FIND_WALL;
      // Serial.println(F("FIND_WALL)"));
      logger.write("c");
      logger.p("c", cycle, unit_count);
    }
    break;
  }

  case COMPASS_CALIBRATE: {
    // look in each direction to make sure the compass has been calibrated
    static float dir = DIR_NORTH - 90 - 360;
    if (car.look(dir)) {
      dir += 90;
    }
    if (dir >= DIR_NORTH) {
      state = FIND_WALL;
      //state = WAYPOINTS;
      Serial.println(F("FIND_WALL)"));
    }

    break;
  }

  case FIND_WALL: {
    float distance = front.lastValue();
    drive.drive(distance / 150.0);

    if (distance < 30) {
      Serial.println(distance);
      drive.stop();

      state = ALIGN_WALL;
      Serial.println(F("ALIGN_WALL)"));
      Serial.println("a)");
    } else {
      front.look();
    }

    break;
  }

  case ALIGN_WALL: {
    if (alignWall(&front, &car, &compass)) {
      state = FOLLOW_WALL;
      Serial.println(F("FOLLOW_WALL)"));
    }
    break;
  }

  case FOLLOW_WALL: {
    if (followWall(&front, &side, &car, &compass)) {
      if (pos != NULL) {
        Position wall = pos->add(compass.getValue() - 90, side.lastMinValue());
        logger.logWall(&wall);
        led_mapped();

        if (isMappingComplete(&wall)) {
          drive.stop();
          state = MAPPING_DONE;
          Serial.println(F("MAPPING_DONE)"));
        }
      }
    }
    break;
  }

  case MAPPING_DONE: {
    static int i = 0;
    if (msTimer.run(150)) {
      led_done(i++);
    }
    break;
  }

  case WAYPOINTS: {
    static int p = 0;
    static Position poss[4] = {Position(0, 0), Position(0, 30),
                               Position(30, 30), Position(30, 0)};

    if (car.driveTo(&poss[p % 4], &myMap.robo)) {
      p++;
    }

    break;
  }
  }

  // serial commands
  char in = Serial.read();
  if (in == 'q') {
    drive.stop();
    if (state == MAPPING_DONE) {
      led_reset();
      Serial.println(F("q -> restart"));
      msTimer.reset();
      state = WAIT_START;
    } else {
      Serial.println(F("q -> quit"));
      state = MAPPING_DONE;
    }
  } else if (in == 's') {
    showStats = !showStats;
  } else if (in == 'l') {
    logger.write(F("test"));
  } else if (in == 'w') {
      state = WAIT_START;
    Serial.println(F("WAYPOINTS)"));
  } else if (in > 0) {
    Serial.print(F("err: "));
    Serial.println(in);
    Serial.println(F("cmds:\n s: stats\n l: log test\n q: quit/restart"));
  }

  resetWatchDogTimer();
}
