#pragma once

#include "compass/compass.h"
#include "config.h"
#include "drive/car.h"
#include "drive/drive.h"
#include "leds.h"
#include "ultrasonic/front.h"
#include <Arduino.h>
#include <timer.h>

typedef enum {
  ALIGN_START,
  ALIGN_CHECK,
  ALIGN_ROTATE,
  ALIGN_DISTANCE,
  ALIGN_ROTATE_2
} align_step;

/**
 * Turn the car to stand parallel to the wall
 * returns if alignment is completed
 */
bool alignWall(Front *front, Car *car, Compass *compass) {
  static align_step alignStep = ALIGN_START;
  static float pos = 0;
  static float min = 9999;
  static float minPos = 0;
  static Timer timer = Timer();
  static float dir = 0;

  switch (alignStep) {
  case ALIGN_START:
    pos = -1.0;
    min = 9999;
    front->look(pos);
    if (timer.run(300)) {
      alignStep = ALIGN_CHECK;
      Serial.println("a.CHECK)");
    }
    return false;

  case ALIGN_CHECK:
    front->look(pos);
    if (timer.run(1)) { // every ms
      if (front->lastValue() < min) {
        min = front->lastValue();
        minPos = pos;
      }
      if (pos < 1) {
        pos += 0.001;
      } else {
        pos = -1;
        front->look(0);
        dir = compass->getValue() + (minPos * (float)FRONT_ANGLE);
        Serial.print("a.Check) min ");
        Serial.println(min);
        alignStep = ALIGN_ROTATE;
        Serial.print("a.ROTATE) dir ");
        Serial.print(compass->getValue());
        Serial.print("+90+45*");
        Serial.print(minPos);
        Serial.print("=");
        Serial.println(dir);
      }
    }
    return false;
  case ALIGN_ROTATE: {
    if (car->look(dir)) {
      alignStep = ALIGN_DISTANCE;
      Serial.println("a.DISTANCE)");
    }
    return false;
  }

  case ALIGN_DISTANCE: {
    float f = front->lastValue();
    if (f > FRONT_DISTANCE_MIN && f < FRONT_DISTANCE_MAX) {
      Drive().stop();
      if (timer.run(500)) {
        alignStep = ALIGN_ROTATE_2;
        Serial.println("a.ROTATE_2)");
      }
    } else {
      timer.reset();
      Drive().drive((f - FRONT_DISTANCE) / FRONT_DISTANCE);
    }
    return false;
  }

  case ALIGN_ROTATE_2: {
    if (car->look(dir + 90)) {
      alignStep = ALIGN_START;
      Serial.println("a.START)");
      return true;
    }
    return false;
  }

  default:
    alignStep = ALIGN_START;
    Serial.println("a.?)");
    return false;
  }
}