#pragma once

#define LED_ENABLE

#define COLORS_CODES                                                           \
  {                                                                            \
    0x00FF00,     /* GREEN */                                                  \
        0xFFFF00, /* YELLOW */                                                 \
        0xFF5500, /* ORANGE */                                                 \
        0xFF0000, /* RED */                                                    \
        0x0000FF, /* BLUE */                                                   \
        0xFFFFFF, /* WHITE */                                                  \
  }
#define COLOR_GREEN 0
#define COLOR_YELLOW 1
#define COLOR_ORANGE 2
#define COLOR_RED 3
#define COLOR_BLUE 4
#define COLOR_WHITE 5
#define COLOR_COUNT 6

#define BRIGHTNESS 10

// Assign LED
#define LED_SD 0
#define LED_REED 1
#define LED_MAP_START 2
#define LED_MAP_COUNT 5
//#define LED_7 7
#define LED_SIDE 8
//#define LED_9 9
#define LED_FRONT 10
//#define LED_11 11

#define LED_COUNT 12

void led_setup(void);

void led_reset(void);
void led_panic(int i);
void led_done(int i);

// indicate that a point was added to the map
void led_mapped(void);
void led_move(void);

void led_front(float i);
void led_side(float i);

void led_toggle(int i);

/**
 * Set a led to a specific color
 */
void led_set(int i, int col);
void led_off(int i);

/**
 * Apply changes to leds
 */
void led_apply(void);
