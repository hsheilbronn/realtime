#pragma once

#include <Arduino.h>

// Front sensor (rewired to make TWI available) and servo
#define PIN_FRONT_TRIGGER A3 // original was A5
#define PIN_FRONT_ECHO A2    // original was A4
#define PIN_FRONT_SERVO 3

// Side sensor (added)
#define PIN_SIDE_TRIGGER A1
#define PIN_SIDE_ECHO A0

// Motor controller L298N
#define PIN_MOTOR_LEFT 5           // ENA
#define PIN_MOTOR_RIGHT 6          // ENB
#define PIN_MOTOR_LEFT_FORWARD 7   // IN1
#define PIN_MOTOR_LEFT_BACKWARD 8  // IN2
#define PIN_MOTOR_RIGHT_BACKWARD 9 // IN4
#define PIN_MOTOR_RIGHT_FORWARD 4  // IN3, original was 11

// Line following sensors (disconnected)
/*
#define PIN_LINE_LEFT 10
#define PIN_LINE_CENTER 4
#define PIN_LINE_RIGHT 2
*/

// IR sensor (hard wired)
#define PIN_IR 12 // same as SD-Card MISO

// Bluetooth
#define PIN_BLUETOOTH_RX 0
#define PIN_BLUETOOTH_TX 1

// Compass sensor
#define PIN_COMPASS_SCL A5
#define PIN_COMPASS_SDA A4

// Reed sensor
#define PIN_REED 2

// SD-Card
#define PIN_SD_CS 10
#define PIN_SD_MOSI 11
#define PIN_SD_MISO 12 // same as IR sensor
#define PIN_SD_CLK 13

// LEDs
#define PIN_LEDS 3 // same as servo of front sensor! ... not nice, but works