
#include "leds.h"
#include "pinconfig.h"
#include <Adafruit_NeoPixel.h>

#ifdef LED_ENABLE
Adafruit_NeoPixel pixels =
    Adafruit_NeoPixel(LED_COUNT, PIN_LEDS, NEO_GRB + NEO_KHZ800);
const unsigned long colors[] = COLORS_CODES;
static bool ledChanged = false;
#endif

void led_setup() {
#ifdef LED_ENABLE
  pixels.begin(); // This initializes the NeoPixel library.
  for (int i = 0; i < LED_COUNT; i++) {
    led_set(i, i);
    led_apply();
    delay(100);
  }
  delay(2000);
  for (int i = 0; i < LED_COUNT; i++) {
    led_off(i);
  }
  led_apply();
#endif
}

/**
 * Set a led to a specific color
 */
void led_set(int i, int col) {
#ifdef LED_ENABLE
  if (pixels.getPixelColor(i % LED_COUNT) != colors[col % COLOR_COUNT]) {
    pixels.setPixelColor(i % LED_COUNT, colors[col % COLOR_COUNT]);
    ledChanged = true;
  }
#endif
}

void led_off(int i) {
#ifdef LED_ENABLE
  pixels.setPixelColor(i % LED_COUNT, 0x000000);
  ledChanged = true;
#endif
}

void led_toggle(int i) { led_set(i, random(0, COLOR_COUNT)); }

void led_sd_error(unsigned char err) { led_set(LED_SD, err); }

// Apply LED changes
// This function is used to limit the amount of communication
// because the data pin is shared with the servo (see. pinconfig.h)
void led_apply(void) {
#ifdef LED_ENABLE
  if (!ledChanged) {
    return;
  }
  ledChanged = false;
  pixels.setBrightness(BRIGHTNESS);
  pixels.show(); // This sends the updated pixel color to the hardware.
#endif
}

// Indicate that a point was added to the map
void led_mapped(void) {
#ifdef LED_ENABLE
  static int points = 0;
  points++;
  for (int i = 0; i < LED_MAP_COUNT; i++) {
    led_off(i + LED_MAP_START);
  }
  led_set((points % LED_MAP_COUNT) + LED_MAP_START, COLOR_BLUE);
#endif
}

// Indicate that mapping has completed
void led_done(int i) {
  for (int l = 0; l < LED_COUNT; l++) {
    led_set(l, COLOR_GREEN);
  }
  led_off(i);
  led_off(i + (LED_COUNT / 2));

  led_apply();
}

// Indicate a serious, possibly unrecoverable error
void led_panic(int i) {
  for (int l = 0; l < LED_COUNT; l++) {
    led_set(l, l + i);
  }
  led_apply();
}

// Turn off all leds
void led_reset(void) {
  for (int l = 0; l < LED_COUNT; l++) {
    led_off(l);
  }
}

void led_front(float i) {
  if (i < 20) {
    led_set(LED_FRONT, COLOR_ORANGE);
  } else if (i > 50) {
    led_set(LED_FRONT, COLOR_RED);
  } else {
    led_set(LED_FRONT, COLOR_GREEN);
  }
}

void led_side(float i) {
  if (i < 20) {
    led_set(LED_SIDE, COLOR_ORANGE);
  } else if (i > 50) {
    led_set(LED_SIDE, COLOR_RED);
  } else {
    led_set(LED_SIDE, COLOR_GREEN);
  }
}