#pragma once

#include "alignwall.h"
#include "config.h"
#include "drive/drive.h"
#include "ultrasonic/front.h"
#include "ultrasonic/side.h"
#include <Arduino.h>

typedef enum {
  FOLLOW_START = 0,
  FOLLOW_DRIVE,
  FOLLOW_REALIGN,
  FOLLOW_ALIGN,
} follow_step;

/**
 * follow the wall
 * @retval true if the robot is driving next to the wall
 * @retval false if robot is realigning with wall
 */
bool followWall(Front *front, Side *side, Car *car, Compass *compass) {
  static unsigned long cycle = 0;
  static follow_step followStep = FOLLOW_START;
  static float dir = 0;
  cycle++;

  float s = side->lastMinValue();
  float f = front->lastValue();

  switch (followStep) {
  case FOLLOW_START: {
    dir = compass->getValue();
    followStep = FOLLOW_DRIVE;
    break;
  }
  case FOLLOW_DRIVE: {
    front->look();
    if (f < FRONT_DISTANCE) {
      Drive().stop();
      followStep = FOLLOW_ALIGN;
      Serial.println(F("f.ALIGN)"));

    } else if (s < SIDE_DISTANCE_MIN) {
      followStep = FOLLOW_REALIGN;
      Serial.println(F("f.REALIGN)"));
    } else if (s > SIDE_DISTANCE_MAX) {
      followStep = FOLLOW_REALIGN;
      Serial.println(F("f.REALIGN)"));
    } else if (!car->look(dir, false)) {
      // wait for car to look in the right direction
    } else {
      float speed = f / 150.0 + 0.2;
      // TODO: angle should also depend on change over time
      // float angle = (SIDE_DISTANCE - s) / -5;
      // Drive().curve(speed, constrain(angle, -0.8, 0.8));
      // dir -= (angle * 0.01);
      Drive().drive(speed);
    }
    break;
  }
  case FOLLOW_REALIGN: {
    front->look(0);
    if (car->look(dir - 90)) {
      followStep = FOLLOW_ALIGN;
      Serial.println(F("f.FIX_DIST)"));
    }
    break;
  }
  case FOLLOW_ALIGN: {
    if (alignWall(front, car, compass)) {
      followStep = FOLLOW_DRIVE;
      dir = compass->getValue();
      Serial.println(F("f.DRIVE)"));
    }
    break;
  }
  default: {
    break;
  }
  }

  return followStep == FOLLOW_DRIVE;
}