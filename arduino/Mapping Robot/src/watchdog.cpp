
/**
 *
 * References:
 * -
 * https://arduino-projekte.webnode.at/registerprogrammierung/watchdog-interrupt/
 * -
 * https://forum.arduino.cc/index.php?action=dlattach;topic=63651.0;attach=3585
 */

#include "watchdog.h"
#include "drive/drive.h"
#include "leds.h"
#include <Arduino.h>
#include <avr/wdt.h>

static unsigned long lastWatchDogReset = 0;

void setupWatchdog(void) {
  cli(); // disable interrupts (equivalent to `SREG &= 0x07`)

  wdt_reset();                       // reset watchdog timer
  MCUSR &= ~(1 << WDRF);             // reset System Reset Flag
  WDTCSR = (1 << WDCE) | (1 << WDE); // start watchdog configuration mode

  // TODO: watchdog timer is set to lowest value to detect errors during
  //       development -> set to higher value for production

  // watchdog timeout at 128kHZ
  // WDTCSR = (0 << WDP3) | (0 << WDP2) | (0 << WDP1) | (0 << WDP0); // 16ms
  // WDTCSR = (0 << WDP3) | (0 << WDP2) | (0 << WDP1) | (1 << WDP0); // 32ms
  // WDTCSR = (0 << WDP3) | (0 << WDP2) | (1 << WDP1) | (0 << WDP0); // 64ms
  // WDTCSR = (0 << WDP3) | (0 << WDP2) | (1 << WDP1) | (1 << WDP0); // 125ms
  WDTCSR = (0 << WDP3) | (1 << WDP2) | (0 << WDP1) | (0 << WDP0); // 250ms
  // WDTCSR = (0 << WDP3) | (1 << WDP2) | (0 << WDP1) | (1 << WDP0); // 500ms
  // WDTCSR = (0 << WDP3) | (1 << WDP2) | (1 << WDP1) | (0 << WDP0); // 1s
  // WDTCSR = (0 << WDP3) | (1 << WDP2) | (1 << WDP1) | (1 << WDP0); // 2s
  // WDTCSR = (1 << WDP3) | (0 << WDP2) | (0 << WDP1) | (0 << WDP0); // 4s
  // WDTCSR = (1 << WDP3) | (0 << WDP2) | (0 << WDP1) | (1 << WDP0); // 5s

  WDTCSR |= (1 << WDIE); // enable watchdog interrupt
  WDTCSR |= (1 << WDE);  // enable reset after watchdog interrupt

  sei(); // enable interrupts

  resetWatchDogTimer(); // Initial reset (to set lastWatchDogReset, etc.)
}

void resetWatchDogTimer(void) {
  wdt_reset();
  lastWatchDogReset = millis();
  return;
}

void disableWatchdog(void) {
  cli(); // disable interrupts (equivalent to `SREG &= 0x07`)
  WDTCSR = (1 << WDCE) | (1 << WDE); // start watchdog configuration mode
  WDTCSR = 0;                        // disable

  sei(); // enable interrupts
}

// Watchdog timer interrupt handler
ISR(WDT_vect) {
  disableWatchdog(); // prevent automatic restart

  int i = 0;
  while (true) {

    // Stop the robot
    Drive().stop();

    // blink leds
    led_panic(i++);
    delay(150);
  }
}