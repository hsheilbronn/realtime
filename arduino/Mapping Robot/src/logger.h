/**
 * Format the SD card under Linux: `mkdosfs -F 32 -I /dev/mmcblk0p1`
 */

#pragma once

//#define ENABLE_SD

#include "map.h"
#include "pinconfig.h"
#include <Arduino.h>
#ifdef ENABLE_SD
#include <SD.h>
#include <SPI.h>
#endif

const char *unit_count = "#";
const char *unit_micros = "µs";
const char *unit_millis = "ms";
const char *unit_cm = "cm";
const char *unit_degree = "°";

#ifdef ENABLE_SD
static const char *log_filename = "LOG";
#endif

class Log {
public:
  Log(void) {}

  bool init(void) {

#ifdef ENABLE_SD
    pinMode(PIN_SD_CS, OUTPUT);
    if (!SD.begin(10)) {
      Serial.println(F("SD err 1"));
      return false;
    };
    if (!SD.remove(log_filename)) {
      Serial.println("SD err 0");
    }
    write("start1");
    const char compile_date[] = __DATE__ " " __TIME__;
    write(compile_date);
    File myFile;

    myFile = SD.open(log_filename);
    if (myFile) {
      Serial.println(log_filename);

      // read from the file until there's nothing else in it:
      while (myFile.available()) {
        Serial.write(myFile.read());
      }
      // close the file:
      myFile.close();
    } else {
      // if the file didn't open, print an error:
      Serial.println(F("SD err 3"));
    }
#endif
    return true;
  }

  void p(String name, float value, String unit) {
    Serial.print(("> "));
    Serial.print(name);
    Serial.print(": ");
    Serial.print(value);
    Serial.println(unit);
  }

  void write(String dataString) {
#ifdef ENABLE_SD
    File myFile;
    myFile = SD.open(log_filename, FILE_WRITE);

    // if the file opened okay, write to it:
    if (myFile) {
      Serial.print(F("SD: "));
      myFile.println(dataString);
      Serial.println(dataString);
      // close the file:
      myFile.close();
      Serial.println(F("done."));
    } else {
      // if the file didn't open, print an error:
      Serial.println(F("SD err 2"));
      // led_sd_error(2);
    }
#else
    Serial.print(F("no SD: "));
    Serial.println(dataString);
#endif
  }

  void logPos(const Position *pos) {
    String r = "$" + String(pos->x) + "," + String(pos->y);
    write(r);
  }

  void logWall(const Position *pos) {
    String w = "|" + String(pos->x) + "," + String(pos->y);
    write(w);
  }
};