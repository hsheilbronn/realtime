#pragma once
#include <Arduino.h>
/**
 * Helper class for non blocking timer
 */

class Timer {
public:
  Timer(unsigned long duration = 1) {
    this->duration = duration;
    this->next = 0;
  }

  /**
   * On the first call the timer is started and returns false.
   * Next calls will return false as long as the time is less than duration
   * defined in timer. When the time exeeds the timer the timer is disabled and
   * true is returned. The timer will only start on the *next* call of this
   * function which will return false again.
   *
   * The second parameter can change the duration of the timer.
   */
  bool run(unsigned long duration = 0) {
    // check if duration changed
    if (duration > 0 && duration != this->duration) {
      // update the current timer
      // this->next = this->next + duration - this->duration;
      // update the timer for further runs
      this->duration = duration;
    }

    if (this->next == 0) {
      // start the timer
      this->next = millis() + this->duration;
    } else if (millis() >= this->next) {
      // timer is done
      this->next = 0;
      return true;
    } else {
      // timer is running
    }
    return false;
  }

  void reset(void) { this->next = 0; }
  void debug(void) {
    Serial.print("t");
    if (this->next == 0) {
      Serial.println(0);
    } else {
      Serial.println(this->next - millis());
    }
  }

private:
  unsigned long duration;
  unsigned long next; // set to 0 when timer is inactive
};
