#pragma once

#include <Arduino.h>

#define MAP_BUFFER_SIZE 2 // This can be reduced if space is running out

class Position {
public:
  Position(void) {
    this->x = 0;
    this->y = 0;
  }

  Position(float x, float y) {
    this->x = x;
    this->y = y;
  }

  // coordinates (float and double have the same precission on Arduino Uno)
  float x;
  float y;

  Position add(float angle, float distance) const;
  void debug(void) const;
};

class Map {
public:
  Map(void) { robo = Position(); }

  const Position *move(float angle, float distance) {
    if (distance == 0) {
      return NULL;
    }
    robo = robo.add(angle, distance);
    return &robo;
  }

  void print(void);

  Position robo;
};

bool isMappingComplete(Position *pos);