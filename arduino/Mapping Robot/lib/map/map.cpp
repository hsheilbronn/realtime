#include "map.h"
#include <Arduino.h>
#include <SD.h>
#include <timer.h>

// Get a new point with a angle [°] and distance [cm] from this point
Position Position::add(float angle, float distance) const {
  float dx = distance * cos(angle * M_PI / 180.0);
  float dy = distance * sin(angle * M_PI / 180.0);

  return Position(this->x + dx, this->y + dy);
}

void Position::debug(void) const {
  Serial.print("(");
  Serial.print(this->x);
  Serial.print(", ");
  Serial.print(this->y);
  Serial.print(")");
}

// Calculate the distance between two points
float dist(Position *a, Position *b) {
  float dX = a->x - b->x;
  float dY = a->y - b->y;
  return sqrt(dX * dX + dY * dY); // a² + b² = c²
}

void Map::print(void) {
  Serial.print("robo: ");
  robo.debug();
  Serial.println("");
}

bool isMappingComplete(Position *pos) {
  static Position ref = Position();
  static Position last = Position();
  static float distance = 0;
  static float maxDiff = 5;
  static Timer msTimer = Timer();
  static bool wasClose = false;

  if (pos == NULL) {
    return false;
  }

  if (ref.x == 0 && ref.y == 0) {
    ref = *pos;
    return false;
  }

  if (msTimer.run(30 * 1000)) {
    // double the allowed distance every 30 seconds
    //      0s 5cm
    //     30s 10cm
    //    1min 20cm
    // 1:30min 40cm
    //    2min 80cm
    // 2:30min 1m60
    //    3min 3m20
    // 3:30min 6m40
    //    4min 12m80
    // 4:30min 25m60
    //    5min 51m20
    maxDiff *= 2;
    Serial.print(F("done?max: "));
    Serial.println(maxDiff);
  }

  // distance between last point and ref
  float lastDist = dist(&ref, &last);
  float refDist = dist(&ref, pos);
  Serial.print(F("done?dist: "));
  Serial.println(refDist);
  // add distance between last and current to total distance
  distance += dist(&last, pos);
  last = *pos;

  if (distance < 150) {
    // we didn't drive 150cm yet so we can't be done
    Serial.print(F("done?way: "));
    Serial.println(distance);
    return false;
  }

  if (refDist < maxDiff) {
    // close to reference position,
    // stop as soon as distance is not getting smaller
    wasClose = true;
    Serial.println(F("done?wasClose"));
  }

  if (lastDist > refDist) {
    // robot is geting closer to reference position,
    // so keep driving until the robot passes the reference position
    Serial.print(F("done?closer: "));
    Serial.print(lastDist);
    Serial.print(F(" > "));
    Serial.println(refDist);
    return false;
  }

  if (!wasClose) {
    // too far from reference position, keep going
    Serial.println(F("done?wasNotClose"));
    return false;
  }

  // all checks above passed -> mapping is completed
  return true;
}