#include <Arduino.h>
#include <map.h>
#include <unity.h>

void test_add_to_empty(void) {
  Map myMap = Map();
  Position pos = Position(1, 2);
  myMap.move(90, 4);
  myMap.print();
  TEST_ASSERT_EQUAL(1, 1);
}

void setup() {
  // NOTE!!! Wait for >2 secs
  // if board doesn't support software reset via Serial.DTR/RTS
  // delay(2000);

  UNITY_BEGIN(); // IMPORTANT LINE!
}

void loop() {
  RUN_TEST(test_add_to_empty);

  UNITY_END(); // stop unit testing
}