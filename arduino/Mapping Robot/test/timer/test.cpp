#include <Arduino.h>
#include <timer.h>
#include <unity.h>

void test_dynamic_timer(void) {
  Timer msTimer = Timer();
  TEST_ASSERT_EQUAL(false, msTimer.run(5));
  delay(3);
  TEST_ASSERT_EQUAL(false, msTimer.run(5));
  delay(3);
  TEST_ASSERT_EQUAL(true, msTimer.run(5));
  delay(3);
  TEST_ASSERT_EQUAL(false, msTimer.run(2)); // timer will restart here
  delay(3);
  TEST_ASSERT_EQUAL(true, msTimer.run(2));
}

void test_fixed_timer(void) {
  Timer msTimer = Timer(5);
  TEST_ASSERT_EQUAL(false, msTimer.run());
  delay(3);
  TEST_ASSERT_EQUAL(false, msTimer.run());
  delay(3);
  TEST_ASSERT_EQUAL(true, msTimer.run());
  TEST_ASSERT_EQUAL(false, msTimer.run()); // timer will restart here
  delay(3);
  TEST_ASSERT_EQUAL(false, msTimer.run());
  delay(3);
  TEST_ASSERT_EQUAL(true, msTimer.run());
}

void setup() {
  // NOTE!!! Wait for >2 secs
  // if board doesn't support software reset via Serial.DTR/RTS
  // delay(2000);

  UNITY_BEGIN(); // IMPORTANT LINE!
}

void loop() {
  RUN_TEST(test_fixed_timer);
  RUN_TEST(test_dynamic_timer);

  UNITY_END(); // stop unit testing
}