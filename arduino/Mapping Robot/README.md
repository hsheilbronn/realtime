# Arduino code for Mappping Robot

## Structure

### core

* `src/main.cpp`: contains the `setup` and `loop` functions
* `src/config.h`: configuration of drive speed and distance to the walls
* `src/pinconfig.h`: pin asignments
* `src/ultrasonic/`: Contains functions to get values from ultrasonic sensors and controll the front servo motor
* `src/compass/`: functions to read compass values
* `src/watchdog.{h|cpp}`: Watchdog timer functions
* `src/leds.h`: functions to set RGB LEDs

### drive functions

* `src/drive/drive.{h|cpp}`: basic drive functions (forward, rotate, curve, ...)
* `src/drive/car.{h|cpp}`: rotate the car based on compass value
* `src/allignwall.h`: functions to align robot with wall
* `src/followwall.h`: functions to drive parallel to the wall

### lib
* `lib/map`: position caluculations and tracking
* `lib/timer`: non-blocking timer class

## Lines Of Code

```bash
$ loc src/ lib/
--------------------------------------------------------------------------------
 Language             Files        Lines        Blank      Comment         Code
--------------------------------------------------------------------------------
 C++                      9         1017          149          117          751
 C/C++ Header            15          669           98           80          491
--------------------------------------------------------------------------------
 Total                   24         1686          247          197         1242
--------------------------------------------------------------------------------
```