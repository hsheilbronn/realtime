# Real Time Systems

## Presentation

Presentation for final presentation:
https://onedrive.live.com/edit.aspx?resid=AD4C000D47EF8498!805&ithint=file%2cpptx&authkey=!AJmKICfBR7V3AkQ

Presentation slide generator:

* https://github.com/marp-team/marp-core


* https://hsheilbronn.gitlab.io/realtime/
    * https://hsheilbronn.gitlab.io/realtime/spi.html
    * https://hsheilbronn.gitlab.io/realtime/mappingrobotidea.html
    * https://hsheilbronn.gitlab.io/realtime/mappingrobot.html


## Documentation

https://docs.google.com/document/d/1DxHx5xXCqaOsDmqI4Mc8GlRzrWXWD5IeYmSeOqPOWuI/edit?usp=sharing

## Tools

Create and simulate arduino circuits:

* https://www.tinkercad.com/


## References

Magnetometer Compass Sensor:

* https://learn.adafruit.com/adafruit-hmc5883l-breakout-triple-axis-magnetometer-compass-sensor/wiring-and-test
* https://circuitdigest.com/microcontroller-projects/digital-compass-with-arduino-and-hmc5883l-magnetometer

Compass calibration:
* https://www.youtube.com/watch?v=QDLk4PcCzWc

Convert polar to cartesian coordinates:

* https://www.mathsisfun.com/polar-cartesian-coordinates.html

Limitations of ultrasonic sensors:

* https://learn.parallax.com/tutorials/cyberbot-roaming-ping/build-ping-bracket-and-circuit

Motor controller:

* https://lastminuteengineers.com/l298n-dc-stepper-driver-arduino-tutorial/#control-pins

## Libraries

Ultrasonic:

* https://github.com/luisllamasbinaburo/Arduino-AsyncSonar ([translate](https://translate.google.com/translate?hl=&sl=es&tl=en&u=https%3A%2F%2Fgithub.com%2Fluisllamasbinaburo%2FArduino-AsyncSonar))

## Diagrams
(click on diagrams to edit in diagrams.net)


[![](https://gitlab.com/hsheilbronn/realtime/-/raw/master/diagrams/MapingRobot.png)](https://app.diagrams.net/#Ahsheilbronn%2Frealtime%2Fmaster%2Fdiagrams%2FMapingRobot.png)

---

[![](https://gitlab.com/hsheilbronn/realtime/-/raw/master/diagrams/FindWall.png)](https://app.diagrams.net/#Ahsheilbronn%2Frealtime%2Fmaster%2Fdiagrams%2FFindWall.png)

---

[![](https://gitlab.com/hsheilbronn/realtime/-/raw/master/diagrams/AllignWall.png)](https://app.diagrams.net/#Ahsheilbronn%2Frealtime%2Fmaster%2Fdiagrams%2FAllignWall.png)

---

[![](https://gitlab.com/hsheilbronn/realtime/-/raw/master/diagrams/FollowWall.png)](https://app.diagrams.net/#Ahsheilbronn%2Frealtime%2Fmaster%2Fdiagrams%2FFollowWall.png)

---

## Maps

![](https://gitlab.com/hsheilbronn/realtime/-/raw/master/svg/template.svg)


![](https://gitlab.com/hsheilbronn/realtime/-/raw/master/svg/template3d.svg)


## Sensors used

In our project, we are using the following sensors
1. Ultrasonic sensors- used to find the wall 
2. Reed sensors- used to measure the distance
3. HMC5883L Compass sensors- used to find the direction 

## Current working state of sensors in our project

Ultrasonic sensors-> working

Reed sensors-> Working

HMC5883L Compass sensors-> First, there was no output.
Later we rectified this problem by connecting the compass sensor in the place of ultrasonic sensors. But even now, we couldnt see any changes in x,y,z magnetic fields value.
